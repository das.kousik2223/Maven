package pers.mv.dep;

import org.apache.commons.lang3.StringUtils;

public class StringConverter {
	public String convertCapital(String input) {
		return StringUtils.capitalize(input);
	}

}
